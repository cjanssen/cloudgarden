Background = Background or {}

function Background.init()
    Background.bg = love.graphics.newImage("img/bg.png")
    Background.border = love.graphics.newImage("img/border.png")
end

function Background.drawBG()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Background.bg)
end

function Background.drawFG()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(Background.border)
end
