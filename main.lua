require 'livecode'


-- cloud life: grow/shrink
-- cloud wandering: random walk + wind
-- plant life: grow/shrink
-- points

function love.load()
    init()
end

function love.livereload()
    init()
end

function init()
    love.filesystem.load("vector.lua")()
    love.filesystem.load("box.lua")()
    love.filesystem.load("timers.lua")()
    love.filesystem.load("city.lua")()
    love.filesystem.load("clouds.lua")()
    love.filesystem.load("sprites.lua")()
    love.filesystem.load("background.lua")()
    love.filesystem.load("factory.lua")()
    love.filesystem.load("sound.lua")()

    -- love.window.setMode(800, 600)
    love.window.setMode(800, 600, { resizable=true, fullscreen=false})
    love.graphics.setDefaultFilter("nearest", "nearest")
    -- love.window.setFullscreen(true, "desktop")
    love.window.setTitle("Cloud Garden")

    Background.init()
    Factory.init()
    City.init()
    Clouds.init()
    Sound.init()

    Sound.playMusic()
end

function love.update(dt)
    Timers.update(dt)
    City.update(dt)
    Clouds.update(dt)
    Factory.update(dt)
end

function love.draw()
    love.graphics.push()
        if love.graphics.getWidth() ~= 800 or love.graphics.getHeight() ~= 600 then
            local screenSize = Vector(love.graphics.getWidth(), love.graphics.getHeight())
            local gameSize = Vector(800, 600)
            local scales = screenSize / gameSize
            local scale = scales.x < scales.y and scales.x or scales.y
            local offset = (screenSize - gameSize * scale) / 2
            love.graphics.translate(offset:unpack())
            love.graphics.scale(scale, scale)
        end
        Background.drawBG()
        City.draw()
        Factory.draw()
        Clouds.draw()
        Background.drawFG()
    love.graphics.pop()
end

function love.keypressed(key)
    if key == "escape" then
        love.event.push("quit")
        return
    end

    if key == "f11" then
        if love.window.getFullscreen() then
            love.window.setMode(800, 600, { resizable=true, fullscreen=false})
            return
        else
            love.window.setFullscreen(true, "desktop")
            return
        end
    end
end

function love.mousepressed(x,y)
    Clouds.press(getMouseCoords())
end

function love.mousereleased()
    Clouds.release()
end


function getMouseCoords()
    local screenSize = Vector(love.graphics.getWidth(), love.graphics.getHeight())
    local gameSize = Vector(800, 600)
    local scales = screenSize / gameSize
    local scale = scales.x < scales.y and scales.x or scales.y
    local offset = (screenSize - gameSize * scale) / 2

    return (Vector(love.mouse.getX(), love.mouse.getY()) - offset) / scale
    -- return Vector(love.mouse.getX(), love.mouse.getY()) * scale + offset

end