function loadSprites(filename, frames_w, frames_h, initial_frame, frame_count, is_looping)
    local container = {}
    local img = love.graphics.newImage(filename)
    container.batch = love.graphics.newSpriteBatch(img, 1000)
    container.sheet = {}
    container.frameCount = frame_count
    container.isLooping = is_looping

    local w,h = math.floor(img:getWidth()/frames_w), math.floor(img:getHeight()/frames_h)
    for i=0,frame_count-1 do
        local x = ((i+initial_frame)%frames_w) * w
        local y = math.floor((i+initial_frame)/frames_w) * h
        table.insert(container.sheet,
            love.graphics.newQuad(x,y,w,h,img:getWidth(),img:getHeight()))
    end

    return container

end