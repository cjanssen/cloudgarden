Clouds = Clouds or {}

local function prepopulate()
    -- local numClouds = 4
    -- for i=1,numClouds do
    --     Clouds.add(Vector(math.random(500) + 150, math.random(400) + 100), Vector())
    -- end
end

function Clouds.init()
    Clouds.list = {}

    -- Clouds.add(Vector(math.random(500) + 150, math.random(400) + 100))
    -- Clouds.add(Vector(math.random(500) + 150, math.random(400) + 100))
    -- Clouds.add(Vector(math.random(500) + 150, math.random(400) + 100))
    -- Clouds.add(Vector(math.random(500) + 150, math.random(400) + 100))
    prepopulate()

    Clouds.initSprites()
end


function Clouds.initSprites()
    Clouds.sprites = {
        norain = loadSprites("img/cloud_norain.png", 7, 1, 0, 7, true),
        shadow = loadSprites("img/cloud_shadow.png", 7, 1, 0, 7, true),
        cloud = loadSprites("img/cloud.png", 8, 1, 0, 8, true),
        cloud_spawn = loadSprites("img/cloud_spawn.png", 6, 1, 0, 6, false),
        cloud_despawn = loadSprites("img/cloud_despawn.png", 9, 1, 0, 9, false),
    }
end

function Clouds.add(pos, lunch)
    local ipos = ((pos - City.offset) / City.cellSide):floor() + Vector(1,1)
    local newCloud = {
        pos = pos,
        ipos = ipos,
        radius = 24,
        dir = lunch:norm(),
        speed = lunch:mod(),
        friction = 0.25,
        isRaining = false,
        frame = 1,
        shadowFrame = 1,
        frameTimer = 0,
        frameDelay = 0.1,
        sprites = Clouds.sprites.cloud_spawn,
        life = 300 + math.random(200),
        spawnTimer = 6*0.1,
    }
    table.insert(Clouds.list, newCloud)
end

function Clouds.update(dt)
    if Clouds.dragging then
        -- Clouds.dragging.time = Clouds.dragging.time - dt
        -- if Clouds.dragging.time <= 0 then
        --     Clouds.dragging = nil
        -- else
            local pos = getMouseCoords() --Vector(love.mouse.getX(), love.mouse.getY())
            local diff = pos - Clouds.dragging.target.pos
            if diff:mod() >= 24 then
                Clouds.dragging.target.dir = diff:norm()
                Clouds.dragging.target.speed = Clouds.dragging.target.speed + math.sqrt(diff:mod()*0.1)
            end

    end

    local despawn_time =  Clouds.sprites.cloud_despawn.frameCount * 0.1

    for _,cloud in ipairs(Clouds.list) do

        -- movement
        cloud.speed = cloud.speed * math.pow(cloud.friction, dt)
        cloud.pos = cloud.pos + cloud.dir * cloud.speed * dt

        -- spatial hash
        local ipos = ((cloud.pos - City.offset) / City.cellSide):floor() + Vector(1,1)
        if not cloud.ipos or not ipos:isEq(cloud.ipos) then
            cloud.ipos = ipos
        end

        -- life
        cloud.life = cloud.life - (cloud.isRaining and 20 or 4) * dt
        if cloud.life <= 0 and not cloud.isDespawning then
            cloud.isDespawning = true
            cloud.despawnTimer = despawn_time
            cloud.frame = 1
        end

        if cloud.isDespawning then
            cloud.despawnTimer = cloud.despawnTimer - dt
        end

        -- animation depending on state
        if cloud.spawnTimer > 0 then
            cloud.spawnTimer = cloud.spawnTimer - dt
            cloud.sprites = Clouds.sprites.cloud_spawn
        elseif cloud.isDespawning then
            cloud.sprites = Clouds.sprites.cloud_despawn
        elseif cloud.isRaining then
            cloud.sprites = Clouds.sprites.cloud
        else
            cloud.sprites = Clouds.sprites.norain
        end

        -- animation
        cloud.frameTimer = cloud.frameTimer + dt
        while cloud.frameTimer > cloud.frameDelay do
            cloud.frameTimer = cloud.frameTimer - cloud.frameDelay
            cloud.frame = cloud.frame + 1
            cloud.shadowFrame = cloud.shadowFrame + 1
        end

        while cloud.frame > cloud.sprites.frameCount do
            if cloud.sprites.isLooping then
                cloud.frame = cloud.frame - cloud.sprites.frameCount
            else
                cloud.frame = cloud.sprites.frameCount
            end
        end

        while cloud.shadowFrame > 7 do
            cloud.shadowFrame = cloud.shadowFrame - 7
        end

    end

    -- remove dead clouds
    for i=#Clouds.list,1,-1 do
        if Clouds.list[i].isDespawning and Clouds.list[i].despawnTimer <= 0 then
            table.remove(Clouds.list, i)
        end
    end

    table.sort(Clouds.list, function(a,b) return a.pos.y < b.pos.y end)
end

function Clouds.draw()
    Clouds.sprites.norain.batch:clear()
    Clouds.sprites.shadow.batch:clear()
    Clouds.sprites.cloud.batch:clear()
    Clouds.sprites.cloud_despawn.batch:clear()
    Clouds.sprites.cloud_spawn.batch:clear()

    for _,cloud in ipairs(Clouds.list) do
        -- love.graphics.setColor(255,255,255)
        -- love.graphics.circle("fill", cloud.pos.x, cloud.pos.y, cloud.radius, cloud.radius)

        local pos = (cloud.pos - Vector(32, 32+64)):floor()
        cloud.sprites.batch:add(cloud.sprites.sheet[cloud.frame], pos.x, pos.y)
        Clouds.sprites.shadow.batch:add(Clouds.sprites.shadow.sheet[cloud.shadowFrame],
            pos.x, pos.y)
    end

    love.graphics.setColor(255,255,255)
    love.graphics.draw(Clouds.sprites.shadow.batch)
    love.graphics.draw(Clouds.sprites.norain.batch)
    love.graphics.draw(Clouds.sprites.cloud.batch)
    love.graphics.draw(Clouds.sprites.cloud_despawn.batch)
    love.graphics.draw(Clouds.sprites.cloud_spawn.batch)
end

function getCloudBox(cloud)
    return CenteredBox(cloud.pos - Vector(0, 32), Vector(64,128))
end

local function getCloudAt(pos)
    for _,cloud in ipairs(Clouds.list) do
        local cloudBox = getCloudBox(cloud)
        if (cloudBox:containsPoint(pos)) then
            return cloud
        end
    end
    return nil
end

function Clouds.press(pos)
    if Clouds.dragging then
        Clouds.dragging.pos = pos:copy()
    else
        local cloud = getCloudAt(pos)
        if cloud then
            Clouds.dragging = {
                target = cloud,
                opos = pos:copy(),
                pos = pos:copy(),
                time = 1
            }
        end
    end
end

function Clouds.release(pos)
    Clouds.dragging = nil
end