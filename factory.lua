Factory = Factory or {}


function Factory.init()

    -- Factory.ipos = Vector(math.random(10),math.random(7))

    Factory.ipos = Vector(6,4)
    Factory.pos = (Factory.ipos - Vector(1,1)) ^ Vector(64,64) + Vector(0.5*(800 - 64*11),600-32-64*8)
    -- Factory.a2c = (Vector(400, 600-32-64*4) - Factory.pos):angle()

    Factory.initSprites()

    Factory.anim = {
        frame = 1,
        frameTimer = 0,
        frameDelay = 0.1,
        frameCount = 24
    }

    Factory.spawnTimer = 0
    Factory.spawnDelay = 0.5
end

function Factory.initSprites()
    Factory.sprites = loadSprites("img/factory.png", 24, 1, 0, 24, true)
end

function Factory.update(dt)
    Factory.anim.frameTimer = Factory.anim.frameTimer + dt
    while Factory.anim.frameTimer > Factory.anim.frameDelay do
        Factory.anim.frameTimer = Factory.anim.frameTimer - Factory.anim.frameDelay
        Factory.anim.frame = Factory.anim.frame + 1
    end

    while Factory.anim.frame > Factory.anim.frameCount do
        Factory.anim.frame = Factory.anim.frame - Factory.anim.frameCount
    end

    local touchDelay = false
    Factory.spawnTimer = Factory.spawnTimer + dt
    while Factory.spawnTimer > Factory.spawnDelay do
        Factory.spawnTimer = Factory.spawnTimer - Factory.spawnDelay
        Clouds.add(Factory.pos + Vector(64,64+32), VectorFromPolar(math.random(100)+100, math.random() * math.pi * 2))
        touchDelay = true
    end

    if touchDelay then
        Factory.spawnDelay = 2 + math.random() * 6
    end
end

function Factory.draw()
    Factory.sprites.batch:clear()

    Factory.sprites.batch:add(Factory.sprites.sheet[Factory.anim.frame], Factory.pos.x, Factory.pos.y)

    love.graphics.setColor(255,255,255)
    love.graphics.draw(Factory.sprites.batch)
end
