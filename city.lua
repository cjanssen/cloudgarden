City = City or {}

local function prepopulate()
    local pool = {}
    for i=1,City.size.x do for j=1,City.size.y do
        local pos = Vector(i,j)
        if (Factory.ipos - pos):mod() > 1.5 then
            table.insert(pool, pos)
        end
    end end

    local numSeeds = 24
    for i=1,numSeeds do
        City.addSeed( table.remove(pool, math.random(#pool)) )
    end
end

function City.init()
    City.size = Vector(11,8)
    City.cellSide = Vector(64, 64)
    City.map = {}
    City.list = {}
    City.offset = Vector(0.5*(800 - 64*11),600-32-64*8)

    -- City.setCell(Vector(3,4),2)
    -- City.setCell(Vector(7,4),3)
    -- City.setCell(Vector(6,8),4)

    -- City.addSeed(Vector(3,4),2)
    -- City.addSeed(Vector(7,4),3)
    -- City.addSeed(Vector(6,8),4)
    prepopulate()

    City.cloudMap = {}
    City.initSprites()
end


function City.initSprites()
    City.sprites = {
        loadSprites("img/buildings2.png", 8, 4, 16, 4, false),
        loadSprites("img/buildings2.png", 8, 4, 24, 4, false),
        seeds_twinkle = loadSprites("img/seeds_twinkle.png", 16, 1, 0, 16, true),
        evo = {
            loadSprites("img/building_evolve_to_small.png", 12, 1, 0 , 12, false),
            loadSprites("img/building_evolve_to_mid.png", 12, 1, 0 , 12, false),
            loadSprites("img/building_evolve_to_large.png", 12, 1, 0 , 12, false),
        }
    }
end

function City.addSeed(ipos)
    local newSeed = {
        ipos = ipos:copy(),
        bldSize = 1,
        bldType = math.random(2),
        growth = 0.25,
        shrink = 0.025,

        frame = math.random(16),
        frameDelay = 0.1,
        frameTimer = 0,

        evoFrame = 1,
        evoTimer = 0,
    }

    table.insert(City.list, newSeed)
    City.setCell(ipos, newSeed)
end

function City.setCell(ipos, cell)
    City.map[ipos.x] = City.map[ipos.x] or {}
    City.map[ipos.x][ipos.y] = cell
end

function City.getCell(ipos)
    return City.map[ipos.x] and City.map[ipos.x][ipos.y]
end

function City.readClouds()
    City.cloudMap = {}
    for _,cloud in ipairs(Clouds.list) do
        cloud.isRaining = false
        City.cloudMap[cloud.ipos.x] = City.cloudMap[cloud.ipos.x] or {}
        City.cloudMap[cloud.ipos.x][cloud.ipos.y] = City.cloudMap[cloud.ipos.x][cloud.ipos.y] or {}
        table.insert(City.cloudMap[cloud.ipos.x][cloud.ipos.y], cloud)
    end
end

function BoxFromPS(pos, size)
    return Box(pos.x,pos.y,pos.x+size.x,pos.y+size.y)
end

local function circleCellCollision(cloud, ipos)
    local s = City.cellSide
    local tl = ipos ^ s + City.offset - s

    local cellBox = BoxFromPS(tl, s)
    -- local cloudBox = getCloudBox(cloud)
    local cloudBox = BoxFromPS(cloud.pos - Vector(32,-8), Vector(32,16))

    return cellBox:intersects(cloudBox)
end

function City.cloudCount(ipos)
    local cnt = 0
    local hsh = {}
    for ix = ipos.x-1,ipos.x+1 do
        for iy = ipos.y-1,ipos.y+1 do
            if City.cloudMap[ix] and City.cloudMap[ix][iy] then
                for _,cloud in ipairs(City.cloudMap[ix][iy]) do
                    if not hsh[cloud] and circleCellCollision(cloud, ipos) then
                        cloud.isRaining = true
                        hsh[cloud] = true
                        cnt = cnt + 1
                    end
                end
            end
        end
    end

    return cnt
end

function City.update(dt)
    City.readClouds()

    for _,seed in ipairs(City.list) do
        local old_bldSize = seed.bldSize
        local cc = City.cloudCount(seed.ipos)
        if cc > 0 then
            seed.bldSize = math.min(seed.bldSize + seed.growth * cc * dt, 5)
        else
            seed.bldSize = math.max(seed.bldSize - seed.shrink * dt, 1)
        end

        if math.floor(old_bldSize) ~= math.floor(seed.bldSize) and seed.bldSize < 5 then
            Sound.playGrow()

            seed.evoTimer = 12 * 0.1
            seed.evoFrame = 1
        end

        if seed.evoTimer > 0 then
            seed.evoTimer = seed.evoTimer - dt
        end


        seed.frameTimer = seed.frameTimer + dt
        while seed.frameTimer > seed.frameDelay do
            seed.frameTimer = seed.frameTimer - seed.frameDelay
            seed.frame = seed.frame + 1
            seed.evoFrame = seed.evoFrame + 1
        end
        while seed.frame > City.sprites.seeds_twinkle.frameCount do
            seed.frame = seed.frame - City.sprites.seeds_twinkle.frameCount
        end

        if seed.evoFrame > 12 then
            seed.evoFrame = 12
        end

    end

end

Color = {
    {128,128,128},
    {192,  0,  0},
    {  0,  0,192},
    {  0,192,  0},
    {  0,192,  0},
}

function City.draw()
    -- for ix=1,City.size.x do
    --     for iy=1,City.size.y do
    --         local col = City.map[ix] and City.map[ix][iy] or 1
    --         love.graphics.setColor(unpack(Color[col]))
    --         local x,y = (ix-1)*City.cellSide.x + City.offset.x, (iy-1)*City.cellSide.y + City.offset.y
    --         love.graphics.rectangle("fill", x, y, City.cellSide.x, City.cellSide.y)
    --     end
    -- end

    -- love.graphics.setColor(Color[1])
    -- love.graphics.rectangle("fill", City.offset.x, City.offset.y, City.cellSide.x * City.size.x, City.cellSide.y * City.size.y)

    love.graphics.setColor(255,255,255)
    City.sprites[1].batch:clear()
    City.sprites[2].batch:clear()
    City.sprites.seeds_twinkle.batch:clear()
    City.sprites.evo[1].batch:clear()
    City.sprites.evo[2].batch:clear()
    City.sprites.evo[3].batch:clear()


    for _,seed in ipairs(City.list) do
        local x,y = (seed.ipos.x-1)*City.cellSide.x + City.offset.x, (seed.ipos.y-2)*City.cellSide.y + City.offset.y

        -- local col = math.floor(seed.bldSize) + 1
        -- love.graphics.setColor(unpack(Color[col]))
        -- love.graphics.rectangle("fill", x, y, City.cellSide.x, City.cellSide.y)

        -- love.graphics.setColor(0,0,0)
        -- love.graphics.print(" "..seed.size, x, y)

        local sz = math.min(4, math.floor(seed.bldSize))
        City.sprites[seed.bldType].batch:add(City.sprites[seed.bldType].sheet[sz],x,y)

        if sz == 1 then
            City.sprites.seeds_twinkle.batch:add(City.sprites.seeds_twinkle.sheet[seed.frame], x, y)
        end

        if seed.evoTimer > 0 then
            local evoNdx = sz-1
            if evoNdx >= 1 and evoNdx <= 3 then
                City.sprites.evo[evoNdx].batch:add(City.sprites.evo[evoNdx].sheet[seed.evoFrame], x, y)
            end
        end
    end

    love.graphics.setColor(255,255,255)
    love.graphics.draw(City.sprites[1].batch)
    love.graphics.draw(City.sprites[2].batch)
    love.graphics.draw(City.sprites.seeds_twinkle.batch)
    love.graphics.draw(City.sprites.evo[1].batch)
    love.graphics.draw(City.sprites.evo[2].batch)
    love.graphics.draw(City.sprites.evo[3].batch)
end