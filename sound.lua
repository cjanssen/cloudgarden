Sound = Sound or {}

Sound.muted = false

function Sound.init()
    love.audio.stop()
    Sound.music = love.audio.newSource("snd/mainloop.ogg","stream")
    Sound.music:setLooping(true)

    Sound.sfx_grow = love.audio.newSource("snd/tree_grows.ogg")
end

function Sound.playMusic()
    if not Sound.muted then
        Sound.music:play()
    end
end

function Sound.playGrow()
    if not Sound.muted then
        Sound.sfx_grow:clone():play()
    end
end